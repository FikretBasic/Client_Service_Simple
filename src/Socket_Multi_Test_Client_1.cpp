//============================================================================
// Name        : Socket_Multi_Test_Client_1.cpp
// Author      : Fikret
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

/**
 *
 *	\mainpage Server application for handling Multiple Client interactions
 *
 *	\author Fikret Basic
 *	\version 0.5
 *	\date 25.02.2018.
 *
 *	\copyright Technical University of Graz - All Rights Reserved
 *
 *
 *	\section intent_sec Overview of the Application
 *
 *	The Application was developed in Eclipse both for Windows and Linux operating system.
 *
 *	\section compile_sec Running the Application
 *	The Application can be run both on the Windows and Linux system. It is only necessary to compile it. \n
 *	After that in "Debug" folder lies the executable or the binary file which can then be run though the terminal command. \n
 *	The program accepts up to two arguments:
 *	- \b arg1 : the server address
 *	- \b arg2 : the connection port
 *
 *	If no argument is given, the default one will be used (for the address 127.0.0.1, and port 8888).
 *
 */

#if defined(_WIN32)

#define _CRT_SECURE_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>

#pragma comment (lib, "Ws2_32.lib")

#else

#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <sstream>

#if !defined (_WINSOCKAPI_)
typedef int SOCKET;
#define INVALID_HANDLE_VALUE -1
#endif /* _WINSOCKAPI_ */

#define PORT 8888

int main(int argc, const char* argv[]) {

	SOCKET clientSocket, ret;
	struct sockaddr_in serverAddr;
	char buffer[1024];

	#if defined(_WIN32)
		WSADATA wsaData;
		int iResult;

		iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			printf("WSAStartup failed with error: %d\n", iResult);
			return 1;
		}
	#endif

	clientSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (clientSocket < 0)
	{
		printf("[-]Error in connection.\n");
		exit(1);
	}
	printf("[+]Client Socket is created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	if (argc > 1)
	{
		serverAddr.sin_addr.s_addr = inet_addr(argv[1]);
	}
	else
	{
		serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");	// htonl(INADDR_ANY);
	}
	if (argc > 2)
	{
		int portIn;
		sscanf(argv[2], "%d", &portIn);
		serverAddr.sin_port = htons(portIn);
	}
	else
	{
		serverAddr.sin_port = htons(PORT);
	}

	ret = connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if (ret < 0)
	{
		printf("[-]Error in connection.\n");
		exit(1);
	}
	printf("[+]Connected to Server.\n");

	while (1)
	{
		printf("Client: \t");
		//scanf("%s", &buffer[0]);
		//std::cin.getline(buffer, sizeof buffer);
		fgets(buffer, sizeof buffer, stdin);
		send(clientSocket, buffer, (int)strlen(buffer) - 1, 0);

		#if defined(_WIN32)
				Sleep(1000);
		#else
				sleep(1);
		#endif

		if (strcmp(buffer, ":exit\n") == 0)
		{
			#if defined (_WIN32)
				closesocket(clientSocket);
				WSACleanup();
			#else
				close(clientSocket);
			#endif

			printf("[-]Disconnected from server.\n");
			exit(1);
		}

		memset(buffer, '\0', sizeof(buffer));

		if (recv(clientSocket, buffer, 1024, 0) < 0)
		{
			printf("[-]Error in receiving data.\n");
		}
		else
		{
			printf("Server: \t%s\n", buffer);
		}
	}

	return 0;
}
